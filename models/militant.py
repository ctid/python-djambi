from .boardCell import BoardCell
from .peon import Peon


class Militant(Peon):
    """The militants can move to one or two squares in the eight directions"""
    def possible_move(self, board, xdest: int, ydest: int):
        xok = 2 >= self.position[0] - xdest >= -2
        yok = 2 >= self.position[1] - ydest >= -2
        return xok and yok and super().possible_move(board, xdest, ydest)

    def after_move(self):
        pass

    def image_path(self):
        return "assets/militant.png"

    def rights(self):
        return self.__doc__
