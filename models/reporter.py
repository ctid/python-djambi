from .boardCell import BoardCell
from .peon import Peon


class Reporter(Peon):
    """the reporter kills by occupying one of the four squares next to the square of the piece he wants to kill
        (he cannot kill diagonally). The corpse stays in his square. The reporter can only kill at the end of his move.
         That means that if he is moved by the diplomat he must move again before killing and so cannot kill a piece
          that is directly orthogonal to him at the beginning of his move. The reporter can move without killing"""
    def possible_move(self, board, xdest: int, ydest: int):
        return super().possible_move(board, xdest, ydest)

    def after_move(self):
        pass

    def image_path(self):
        return "assets/reporter.png"

    def rights(self):
        return self.__doc__
