from .boardCell import BoardCell
from .peon import Peon


class Necromobile(Peon):
    """the necromobile acts like a diplomat but only with the dead pieces (whatever the origin of the dead piece is).
     The corpses cannot be placed in the maze."""
    def possible_move(self, board, xdest: int, ydest: int):
        return super().possible_move(board, xdest, ydest)

    def after_move(self):
        pass

    def image_path(self):
        return "assets/necromobile.png"

    def rights(self):
        return self.__doc__
