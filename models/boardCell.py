from tkinter import *
from PIL import Image, ImageTk

from models._colors import Colors
from models._constants import CELL_SIZE, BLANK_IMG_PATH, CONVERT_TYPE_RGBA


class BoardCell:
    """This class represent a cell of the boards, in each cell you can find multiple peons"""
    def __init__(self, peons: []):
        self._peons = peons
        self._btn = None

    @property
    def btn(self):
        return self._btn

    @property
    def peons(self) -> []:
        return self._peons

    @peons.setter
    def peons(self, peons: []):
        self._peons = peons

    def draw(self, window: Tk, x: int, y: int, board):
        """Called to draw peon that are on a board cell"""
        if self.peons and self.peons[0]:
            peon = self.peons[0]
            image_source = Image.open(peon.image_path())
            image_source = image_source.resize((CELL_SIZE, CELL_SIZE))
            image = ImageTk.PhotoImage(image_source)
            btn_color = peon.color
        else:
            image_source = Image.open(BLANK_IMG_PATH).convert(CONVERT_TYPE_RGBA)
            image_source = image_source.resize((CELL_SIZE, CELL_SIZE))
            image = ImageTk.PhotoImage(image_source)
            btn_color = Colors.WHITE

        self._btn = Button(window, image=image, highlightbackground=btn_color, bg=btn_color,
                           command=lambda: board.onCellClicked(x, y, window))
        self._btn.image = image  # beaucause of carbage collector
        self._btn.grid(row=x, column=y)


