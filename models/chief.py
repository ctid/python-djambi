from .boardCell import BoardCell
from .peon import Peon


class Chief(Peon):
    """the chief kills and places the corpse in the same way as the militant."""
    def possible_move(self, board, xdest: int, ydest: int):
        return super().possible_move(board, xdest, ydest)

    def after_move(self):
        pass

    def image_path(self):
        return "assets/chief.png"

    def rights(self):
        return self.__doc__
