from abc import ABC, abstractmethod
from models._colors import Colors


class Peon(ABC):
    """Peon is an abstract class used by all peons"""
    def __init__(self, xy: (int, int), color: Colors):
        self._is_alive = True
        self._position = xy
        self._color = color

    def __repr__(self):
        return type(self).__name__

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, color: Colors):
        self._color = color

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, position: (int, int)):
        self._position = position

    @abstractmethod
    def possible_move(self, board, xdest: int, ydest: int):
        """Return a boolean depending on peon move abilities"""
        peonsOnCell = board.cells[xdest][ydest].peons

        if len(peonsOnCell) == 0 or peonsOnCell[0] is None:
            return True

        if peonsOnCell[0].color != self.color:
            return True

        return False

    @abstractmethod
    def after_move(self):
        """Called after a peon is moved, some peons have special after move abilites"""
        pass

    @abstractmethod
    def image_path(self):
        """Path to icon that will represent the peon on the board"""
        pass

    @abstractmethod
    def rights(self):
        """Returns a string containing every peons move restriction"""
        pass
