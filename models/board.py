from tkinter import messagebox

from .boardCell import BoardCell
from .chief import Chief
from .diplomat import Diplomat
from .militant import Militant
from .necromobile import Necromobile
from .reporter import Reporter
from .assassin import Assassin
from ._colors import Colors
from tkinter import *


class Board:
    """The board class is where the peons will be place, it will control the movement of peons and th rendering """
    def __init__(self, dimensions: (int, int)):
        self.dimensions = dimensions
        cells = []
        for x in range(self.dimensions[0]):
            row = []
            for y in range(self.dimensions[1]):
                row.append(BoardCell([]))
            cells.append(row)
        self.cells = cells
        self._selectedCell = None
        self.generateBoard()

    def generateBoard(self):
        """places all peons on board"""
        # TODO : refactor generateBoard
        # region Greens
        # first line
        self.cells[0][0].peons = [Chief((0, 0), Colors.GREEN)]
        self.cells[0][1].peons = [Assassin((0, 1), Colors.GREEN)]
        self.cells[0][2].peons = [Militant((0, 2), Colors.GREEN)]

        # second line
        self.cells[1][0].peons = [Reporter((1, 0), Colors.GREEN)]
        self.cells[1][1].peons = [Diplomat((1, 1), Colors.GREEN)]
        self.cells[1][2].peons = [Militant((1, 2), Colors.GREEN)]

        # third lin
        self.cells[2][0].peons = [Militant((2, 0), Colors.GREEN)]
        self.cells[2][1].peons = [Militant((2, 1), Colors.GREEN)]
        self.cells[2][2].peons = [Necromobile((2, 2), Colors.GREEN)]
        # endregion
        # region Yellows
        # first line
        self.cells[0][6].peons = [Militant((0, 6), Colors.YELLOW)]
        self.cells[0][7].peons = [Assassin((0, 7), Colors.YELLOW)]
        self.cells[0][8].peons = [Chief((0, 8), Colors.YELLOW)]

        # second line
        self.cells[1][6].peons = [Militant((1, 6), Colors.YELLOW)]
        self.cells[1][7].peons = [Diplomat((1, 7), Colors.YELLOW)]
        self.cells[1][8].peons = [Militant((1, 8), Colors.YELLOW)]

        # third line
        self.cells[2][6].peons = [Necromobile((2, 6), Colors.YELLOW)]
        self.cells[2][7].peons = [Militant((2, 7), Colors.YELLOW)]
        self.cells[2][8].peons = [Militant((2, 8), Colors.YELLOW)]
        # endregion
        # region Reds
        # first line
        self.cells[6][0].peons = [Militant((6, 0), Colors.RED)]
        self.cells[6][1].peons = [Militant((6, 1), Colors.RED)]
        self.cells[6][2].peons = [Necromobile((6, 2), Colors.RED)]

        # second line
        self.cells[7][0].peons = [Reporter((7, 0), Colors.RED)]
        self.cells[7][1].peons = [Diplomat((7, 1), Colors.RED)]
        self.cells[7][2].peons = [Militant((7, 2), Colors.RED)]

        # third line
        self.cells[8][0].peons = [Chief((8, 0), Colors.RED)]
        self.cells[8][1].peons = [Assassin((8, 1), Colors.RED)]
        self.cells[8][2].peons = [Militant((8, 2), Colors.RED)]
        # endregion
        # region Blues
        # first line
        self.cells[6][6].peons = [Necromobile((6, 6), Colors.BLUE)]
        self.cells[6][7].peons = [Militant((6, 7), Colors.BLUE)]
        self.cells[6][8].peons = [Militant((6, 8), Colors.BLUE)]

        # second line
        self.cells[7][6].peons = [Militant((7, 6), Colors.BLUE)]
        self.cells[7][7].peons = [Diplomat((7, 7), Colors.BLUE)]
        self.cells[7][8].peons = [Reporter((7, 8), Colors.BLUE)]

        # third line
        self.cells[8][6].peons = [Militant((8, 6), Colors.BLUE)]
        self.cells[8][7].peons = [Assassin((8, 7), Colors.BLUE)]
        self.cells[8][8].peons = [Chief((8, 8), Colors.BLUE)]
        # endregion

    def move(self, xdest: int, ydest: int, window: Tk):
        """called when trying to move a peon"""
        peonToMove = self._selectedCell.peons[0]
        if peonToMove is not None and peonToMove.possible_move(self, xdest, ydest):
            oldPeonPosition = peonToMove.position

            peonToMove.position = (xdest, ydest)
            self.cells[xdest][ydest].peons = [peonToMove]
            self.cells[xdest][ydest].draw(window, xdest, ydest, self)

            self.cleanCell(window, oldPeonPosition)
        else:
            messagebox.showinfo("You can't move there", peonToMove.rights())

    def cleanCell(self, window: Tk, old_peon_position: []):
        """cleans the old cell after move"""
        self._selectedCell.peons[0] = None
        self._selectedCell.draw(window, old_peon_position[0], old_peon_position[1], self)

    def onCellClicked(self, x: int, y: int, window: Tk):
        """called when a cell is clicked"""
        if self._selectedCell is None:
            peons = self.cells[x][y].peons
            if len(peons) > 0:
                self._selectedCell = self.cells[x][y]
                # TODO : change selected btns color
                #self._selectedCell.btn.highlightbackground = Colors.WHITE
        else:
            self.move(x, y, window)
            self._selectedCell = None

    def draw(self, window: Tk):
        """called to draw the board with TK"""
        for i, row in enumerate(self.cells):
            for j, cell in enumerate(row):
                cell.draw(window, i, j, self)
