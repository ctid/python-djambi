class Colors:
    """Color constants"""
    RED = "#A60000"
    YELLOW = "#E5BF00"
    BLUE = "#0000B2"
    GREEN = "#004F19"
    WHITE = "#FFFFFF"
