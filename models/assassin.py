from .peon import Peon


class Assassin(Peon):
    """the assassin kills in the same way as the militant, but places the corpse in the square he comes from.
    This is the symbol that corpses by assassin tend to mess space at home in politics."""
    def possible_move(self, board, xdest: int, ydest: int):
        return super().possible_move(board, xdest, ydest)

    def after_move(self):
        pass

    def image_path(self):
        return "assets/assassin.png"

    def rights(self):
        return self.__doc__
