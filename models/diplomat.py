from .boardCell import BoardCell
from .peon import Peon


class Diplomat(Peon):
    """the diplomat can move another living piece by occupying its square
    (of course, he can only move the pieces of the other players).
    The piece is placed on any unoccupied square (except the maze if this piece is not a chief)."""
    def possible_move(self, board, xdest: int, ydest: int):
        return super().possible_move(board, xdest, ydest)

    def after_move(self):
        pass

    def image_path(self):
        return "assets/diplomate.png"

    def rights(self):
        return self.__doc__
