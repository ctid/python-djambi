from tkinter import *
from models.board import Board


if __name__ == '__main__':

    window = Tk()  # start tk windows
    board = Board((9, 9))  # initalize board
    board.draw(window)  # draw board and game is started !
    window.mainloop()  # show tk window
