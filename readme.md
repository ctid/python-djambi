# Python - Djambi

Python - Djambi is a remake of the Djambi board game in python. 

As [wikipedia](https://en.wikipedia.org/wiki/Djambi) says Djambi (also described as "Machiavelli's chessboard") is a board game and a chess variant for four players, invented by Jean Anesto in 1975. The rulebook in French describes the game, the pieces and the rules in a humorous and theatrical way, clearly stating that the game pieces are intended to represent all wrongdoings in politics.

## Start the game

### IDE
You can launch the using any IDE that supports python like PyCharm 

### Bash
Or you can launch the game using the following commands
```bash
cd python-djambi
python main.py
```

## Enjoy
Game still in developpement so please be nice